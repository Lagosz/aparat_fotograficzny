/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TrybSerwisowy
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/TrybSerwisowy.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "TrybSerwisowy.h"
//#[ ignore
#define Default_TrybSerwisowy_TrybSerwisowy_SERIALIZE OM_NO_OP

#define Default_TrybSerwisowy_getOptions_SERIALIZE OM_NO_OP

#define Default_TrybSerwisowy_off_SERIALIZE OM_NO_OP

#define Default_TrybSerwisowy_on_SERIALIZE OM_NO_OP

#define Default_TrybSerwisowy_setOptions_SERIALIZE aomsmethod->addAttribute("options", x2String(options));
//#]

//## package Default

//## class TrybSerwisowy
TrybSerwisowy::TrybSerwisowy() {
    NOTIFY_CONSTRUCTOR(TrybSerwisowy, TrybSerwisowy(), 0, Default_TrybSerwisowy_TrybSerwisowy_SERIALIZE);
}

TrybSerwisowy::~TrybSerwisowy() {
    NOTIFY_DESTRUCTOR(~TrybSerwisowy, false);
}

RhpString TrybSerwisowy::getOptions() {
    NOTIFY_OPERATION(getOptions, getOptions(), 0, Default_TrybSerwisowy_getOptions_SERIALIZE);
    //#[ operation getOptions()
    //#]
}

bool TrybSerwisowy::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_TrybSerwisowy_off_SERIALIZE);
    //#[ operation off()
    //#]
}

bool TrybSerwisowy::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_TrybSerwisowy_on_SERIALIZE);
    //#[ operation on()
    //#]
}

bool TrybSerwisowy::setOptions(const RhpString& options) {
    NOTIFY_OPERATION(setOptions, setOptions(const RhpString&), 1, Default_TrybSerwisowy_setOptions_SERIALIZE);
    //#[ operation setOptions(RhpString)
    //#]
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedTrybSerwisowy::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedTrybSerwisowy::serializeRelations(AOMSRelations* aomsRelations) const {
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(TrybSerwisowy, Default, false, Modul, OMAnimatedModul, OMAnimatedTrybSerwisowy)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TrybSerwisowy.cpp
*********************************************************************/
