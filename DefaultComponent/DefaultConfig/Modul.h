/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Modul.h
*********************************************************************/

#ifndef Modul_H
#define Modul_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class Modul
class Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedModul;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Modul();
    
    //## auto_generated
    virtual ~Modul();
    
    ////    Operations    ////
    
    //## operation getOptions()
    virtual RhpString getOptions();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOptions(RhpString)
    virtual bool setOptions(const RhpString& options);
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getAvability() const;
    
    //## auto_generated
    void setAvability(int p_avability);

private :

    //## auto_generated
    int getID() const;
    
    //## auto_generated
    void setID(int p_ID);
    
    //## auto_generated
    int getNazwa() const;
    
    //## auto_generated
    void setNazwa(int p_nazwa);
    
    ////    Attributes    ////

protected :

    int ID;		//## attribute ID
    
    int avability;		//## attribute avability
    
    int nazwa;		//## attribute nazwa
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedModul : virtual public AOMInstance {
    DECLARE_META(Modul, OMAnimatedModul)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.h
*********************************************************************/
