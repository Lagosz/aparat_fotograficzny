/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Aparat
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Aparat.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Aparat.h"
//## link itsPamiec
#include "Pamiec.h"
//## link itsParametry
#include "Parametry.h"
//#[ ignore
#define Default_Aparat_Aparat_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Aparat
Aparat::Aparat(IOxfActive* theActiveContext) : dane(0), danePrzetworzone(0) {
    NOTIFY_ACTIVE_CONSTRUCTOR(Aparat, Aparat(), 0, Default_Aparat_Aparat_SERIALIZE);
    setActiveContext(this, true);
    {
        {
            itsSterownik_2.setShouldDelete(false);
        }
    }
    itsPamiec = NULL;
    initRelations();
    initStatechart();
}

Aparat::~Aparat() {
    NOTIFY_DESTRUCTOR(~Aparat, true);
    cleanUpRelations();
}

int Aparat::getDane() const {
    return dane;
}

void Aparat::setDane(int p_dane) {
    dane = p_dane;
    NOTIFY_SET_OPERATION;
}

int Aparat::getDanePrzetworzone() const {
    return danePrzetworzone;
}

void Aparat::setDanePrzetworzone(int p_danePrzetworzone) {
    danePrzetworzone = p_danePrzetworzone;
    NOTIFY_SET_OPERATION;
}

Pamiec* Aparat::getItsPamiec() const {
    return itsPamiec;
}

void Aparat::setItsPamiec(Pamiec* p_Pamiec) {
    itsPamiec = p_Pamiec;
    if(p_Pamiec != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsPamiec", p_Pamiec, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsPamiec");
        }
}

OMIterator<Parametry*> Aparat::getItsParametry() const {
    OMIterator<Parametry*> iter(itsParametry);
    return iter;
}

Parametry* Aparat::newItsParametry() {
    Parametry* newParametry = new Parametry;
    newParametry->_setItsAparat(this);
    itsParametry.add(newParametry);
    NOTIFY_RELATION_ITEM_ADDED("itsParametry", newParametry, true, false);
    return newParametry;
}

void Aparat::deleteItsParametry(Parametry* p_Parametry) {
    p_Parametry->_setItsAparat(NULL);
    itsParametry.remove(p_Parametry);
    NOTIFY_RELATION_ITEM_REMOVED("itsParametry", p_Parametry);
    delete p_Parametry;
}

Sterownik* Aparat::getItsSterownik_2() const {
    return (Sterownik*) &itsSterownik_2;
}

bool Aparat::startBehavior() {
    bool done = true;
    done &= itsSterownik_2.startBehavior();
    done &= OMReactive::startBehavior();
    if(done)
        {
            startDispatching();
        }
    return done;
}

void Aparat::initRelations() {
    itsSterownik_2._setItsAparat_2(this);
}

void Aparat::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    Proces_subState = OMNonState;
    state_12_subState = OMNonState;
    state_12_active = OMNonState;
    Wyswietlanie_subState = OMNonState;
    state_11_subState = OMNonState;
    state_11_active = OMNonState;
}

void Aparat::cleanUpRelations() {
    {
        OMIterator<Parametry*> iter(itsParametry);
        while (*iter){
            deleteItsParametry(*iter);
            iter.reset();
        }
    }
    if(itsPamiec != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsPamiec");
            itsPamiec = NULL;
        }
}

void Aparat::_addItsParametry(Parametry* p_Parametry) {
    if(p_Parametry != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsParametry", p_Parametry, false, false);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsParametry");
        }
    itsParametry.add(p_Parametry);
}

void Aparat::_removeItsParametry(Parametry* p_Parametry) {
    NOTIFY_RELATION_ITEM_REMOVED("itsParametry", p_Parametry);
    itsParametry.remove(p_Parametry);
}

void Aparat::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsSterownik_2.setActiveContext(theActiveContext, false);
    }
}

void Aparat::destroy() {
    itsSterownik_2.destroy();
    OMReactive::destroy();
}

void Aparat::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("16");
        NOTIFY_STATE_ENTERED("ROOT.Testowanie");
        rootState_subState = Testowanie;
        rootState_active = Testowanie;
        //#[ state Testowanie.(Entry) 
        std::cout << std::endl << "Testowanie";
        //#]
        NOTIFY_TRANSITION_TERMINATED("16");
    }
}

IOxfReactive::TakeEventStatus Aparat::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Testowanie
        case Testowanie:
        {
            if(IS_EVENT_TYPE_OF(evTest_Default_id))
                {
                    //## transition 1 
                    if(rejestrAwarii==0)
                        {
                            NOTIFY_TRANSITION_STARTED("0");
                            NOTIFY_TRANSITION_STARTED("1");
                            NOTIFY_STATE_EXITED("ROOT.Testowanie");
                            NOTIFY_STATE_ENTERED("ROOT.WyborTrybu");
                            rootState_subState = WyborTrybu;
                            rootState_active = WyborTrybu;
                            //#[ state WyborTrybu.(Entry) 
                            std::cout << std::endl << "Dostepne operacje: ";
                            std::cout << std::endl << "Tryb Serwisowy";
                            std::cout << std::endl << "Proces"; 
                            
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("1");
                            NOTIFY_TRANSITION_TERMINATED("0");
                            res = eventConsumed;
                        }
                    else
                        {
                            NOTIFY_TRANSITION_STARTED("0");
                            NOTIFY_TRANSITION_STARTED("2");
                            NOTIFY_STATE_EXITED("ROOT.Testowanie");
                            NOTIFY_STATE_ENTERED("ROOT.Awaria");
                            rootState_subState = Awaria;
                            rootState_active = Awaria;
                            //#[ state Awaria.(Entry) 
                            std::cout << std::endl << "AWARIA!";
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("2");
                            NOTIFY_TRANSITION_TERMINATED("0");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State TrybSerwisowy
        case TrybSerwisowy:
        {
            if(IS_EVENT_TYPE_OF(evModernizacja_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    NOTIFY_STATE_EXITED("ROOT.TrybSerwisowy");
                    NOTIFY_STATE_ENTERED("ROOT.Modernizacja");
                    rootState_subState = Modernizacja;
                    rootState_active = Modernizacja;
                    //#[ state Modernizacja.(Entry) 
                    std::cout << std::endl << "Modernizacja";
                    
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evDiagnostyka_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    NOTIFY_STATE_EXITED("ROOT.TrybSerwisowy");
                    NOTIFY_STATE_ENTERED("ROOT.Diagnostyka");
                    rootState_subState = Diagnostyka;
                    rootState_active = Diagnostyka;
                    //#[ state Diagnostyka.(Entry) 
                    std::cout << std::endl << "Diagnostyka ";
                     
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
        }
        break;
        // State WyborTrybu
        case WyborTrybu:
        {
            if(IS_EVENT_TYPE_OF(evProces_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    NOTIFY_STATE_EXITED("ROOT.WyborTrybu");
                    Proces_entDef();
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evTrybSerwisowy_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_STATE_EXITED("ROOT.WyborTrybu");
                    NOTIFY_STATE_ENTERED("ROOT.TrybSerwisowy");
                    rootState_subState = TrybSerwisowy;
                    rootState_active = TrybSerwisowy;
                    //#[ state TrybSerwisowy.(Entry) 
                    std::cout << std::endl << "Dostepne operacje: ";
                    std::cout << std::endl << "Modernizacja";
                    std::cout << std::endl << "Diagnostyka"; 
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Modernizacja
        case Modernizacja:
        {
            if(IS_EVENT_TYPE_OF(evTest_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    NOTIFY_STATE_EXITED("ROOT.Modernizacja");
                    NOTIFY_STATE_ENTERED("ROOT.Testowanie");
                    rootState_subState = Testowanie;
                    rootState_active = Testowanie;
                    //#[ state Testowanie.(Entry) 
                    std::cout << std::endl << "Testowanie";
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Diagnostyka
        case Diagnostyka:
        {
            if(IS_EVENT_TYPE_OF(evDiagnostyka_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("18");
                    NOTIFY_STATE_EXITED("ROOT.Diagnostyka");
                    NOTIFY_STATE_ENTERED("ROOT.TrybSerwisowy");
                    rootState_subState = TrybSerwisowy;
                    rootState_active = TrybSerwisowy;
                    //#[ state TrybSerwisowy.(Entry) 
                    std::cout << std::endl << "Dostepne operacje: ";
                    std::cout << std::endl << "Modernizacja";
                    std::cout << std::endl << "Diagnostyka"; 
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("18");
                    res = eventConsumed;
                }
            
        }
        break;
        // State NacisniecieSpustuMigawki
        case NacisniecieSpustuMigawki:
        {
            if(IS_EVENT_TYPE_OF(evNacisnijMigawke_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    NOTIFY_STATE_EXITED("ROOT.Proces.NacisniecieSpustuMigawki");
                    NOTIFY_STATE_ENTERED("ROOT.Proces.PobranieDanych");
                    pushNullTransition();
                    Proces_subState = PobranieDanych;
                    rootState_active = PobranieDanych;
                    //#[ state Proces.PobranieDanych.(Entry) 
                    std::cout << std::endl << "pobieranie Danych";
                    dane=rand() % 90000 + 10000;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State PobranieDanych
        case PobranieDanych:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("9");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Proces.PobranieDanych");
                    NOTIFY_STATE_ENTERED("ROOT.Proces.PrzetworzenieDanych");
                    pushNullTransition();
                    Proces_subState = PrzetworzenieDanych;
                    rootState_active = PrzetworzenieDanych;
                    //#[ state Proces.PrzetworzenieDanych.(Entry) 
                    std::cout << std::endl << "Przetwarzanie Danych";
                    danePrzetworzone=dane*5;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("9");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State PrzetworzenieDanych
        case PrzetworzenieDanych:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("14");
                    NOTIFY_TRANSITION_STARTED("10");
                    NOTIFY_TRANSITION_STARTED("11");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Proces.PrzetworzenieDanych");
                    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10");
                    Proces_subState = state_10;
                    rootState_active = state_10;
                    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_12");
                    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_11");
                    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_11.ZapisObrazu");
                    state_11_subState = ZapisObrazu;
                    state_11_active = ZapisObrazu;
                    //#[ state Proces.state_10.state_11.ZapisObrazu.(Entry) 
                    std::cout << std::endl << "Zapis Obrazu";
                    zdjecie[i]=danePrzetworzone;
                    
                    //#]
                    Wyswietlanie_entDef();
                    NOTIFY_TRANSITION_TERMINATED("11");
                    NOTIFY_TRANSITION_TERMINATED("10");
                    NOTIFY_TRANSITION_TERMINATED("14");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State state_10
        case state_10:
        {
            res = state_10_processEvent();
        }
        break;
        
        default:
            break;
    }
    return res;
}

void Aparat::Proces_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Proces");
    rootState_subState = Proces;
    NOTIFY_TRANSITION_STARTED("15");
    NOTIFY_STATE_ENTERED("ROOT.Proces.NacisniecieSpustuMigawki");
    Proces_subState = NacisniecieSpustuMigawki;
    rootState_active = NacisniecieSpustuMigawki;
    //#[ state Proces.NacisniecieSpustuMigawki.(Entry) 
    std::cout << std::endl << "Nacisnij Spust migawki";
    //#]
    NOTIFY_TRANSITION_TERMINATED("15");
}

void Aparat::Proces_exit() {
    switch (Proces_subState) {
        // State NacisniecieSpustuMigawki
        case NacisniecieSpustuMigawki:
        {
            NOTIFY_STATE_EXITED("ROOT.Proces.NacisniecieSpustuMigawki");
        }
        break;
        // State PobranieDanych
        case PobranieDanych:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Proces.PobranieDanych");
        }
        break;
        // State PrzetworzenieDanych
        case PrzetworzenieDanych:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Proces.PrzetworzenieDanych");
        }
        break;
        // State state_10
        case state_10:
        {
            state_10_exit();
        }
        break;
        // State terminationstate_16
        case terminationstate_16:
        {
            NOTIFY_STATE_EXITED("ROOT.Proces.terminationstate_16");
        }
        break;
        default:
            break;
    }
    Proces_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Proces");
}

void Aparat::state_10_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10");
    Proces_subState = state_10;
    rootState_active = state_10;
    state_11_entDef();
    state_12_entDef();
}

void Aparat::state_10_exit() {
    state_11_exit();
    state_12_exit();
    
    NOTIFY_STATE_EXITED("ROOT.Proces.state_10");
}

IOxfReactive::TakeEventStatus Aparat::state_10_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State state_11
    if(state_11_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(state_10))
                {
                    return res;
                }
        }
    // State state_12
    if(state_12_processEvent() != eventNotConsumed)
        {
            res = eventConsumed;
            if(!IS_IN(state_10))
                {
                    return res;
                }
        }
    
    return res;
}

void Aparat::state_12_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_12");
    Wyswietlanie_entDef();
}

void Aparat::state_12_exit() {
    // State Wyswietlanie
    if(state_12_subState == Wyswietlanie)
        {
            switch (Wyswietlanie_subState) {
                // State Wyswietl
                case Wyswietl:
                {
                    NOTIFY_STATE_EXITED("ROOT.Proces.state_10.state_12.Wyswietlanie.ROOT.Wyswietlanie.Wyswietl");
                }
                break;
                // State terminationstate_18
                case terminationstate_18:
                {
                    NOTIFY_STATE_EXITED("ROOT.Proces.state_10.state_12.Wyswietlanie.ROOT.Wyswietlanie.terminationstate_18");
                }
                break;
                default:
                    break;
            }
            Wyswietlanie_subState = OMNonState;
            //#[ state Proces.state_10.state_12.Wyswietlanie.(Exit) 
            dane=0;
            danePrzetworzone=0;
            i++;
            //#]
            NOTIFY_STATE_EXITED("ROOT.Proces.state_10.state_12.Wyswietlanie");
        }
    state_12_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Proces.state_10.state_12");
}

IOxfReactive::TakeEventStatus Aparat::state_12_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (state_12_active) {
        // State Wyswietl
        case Wyswietl:
        {
            res = Wyswietl_handleEvent();
        }
        break;
        // State terminationstate_18
        case terminationstate_18:
        {
            res = Wyswietlanie_handleEvent();
        }
        break;
        default:
            break;
    }
    return res;
}

void Aparat::Wyswietlanie_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_12.Wyswietlanie");
    state_12_subState = Wyswietlanie;
    //#[ state Proces.state_10.state_12.Wyswietlanie.(Entry) 
    std::cout << std::endl << "Wyswietlanie Obrazu";
    //#]
    NOTIFY_TRANSITION_STARTED("ROOT.Proces.state_10.state_12.Wyswietlanie.0");
    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_12.Wyswietlanie.ROOT.Wyswietlanie.Wyswietl");
    Wyswietlanie_subState = Wyswietl;
    state_12_active = Wyswietl;
    NOTIFY_TRANSITION_TERMINATED("ROOT.Proces.state_10.state_12.Wyswietlanie.0");
}

IOxfReactive::TakeEventStatus Aparat::Wyswietlanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evNoweZdjecie_Default_id))
        {
            //## transition 17 
            if((IS_IN(ZapisObrazu)) && (IS_COMPLETED(Wyswietlanie)==true))
                {
                    NOTIFY_TRANSITION_STARTED("17");
                    NOTIFY_TRANSITION_STARTED("12");
                    NOTIFY_TRANSITION_STARTED("13");
                    state_10_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Proces.terminationstate_16");
                    Proces_subState = terminationstate_16;
                    rootState_active = terminationstate_16;
                    NOTIFY_TRANSITION_TERMINATED("13");
                    NOTIFY_TRANSITION_TERMINATED("12");
                    NOTIFY_TRANSITION_TERMINATED("17");
                    res = eventConsumed;
                }
        }
    
    
    return res;
}

IOxfReactive::TakeEventStatus Aparat::Wyswietl_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evNoweZdjecie_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("ROOT.Proces.state_10.state_12.Wyswietlanie.1");
            NOTIFY_STATE_EXITED("ROOT.Proces.state_10.state_12.Wyswietlanie.ROOT.Wyswietlanie.Wyswietl");
            NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_12.Wyswietlanie.ROOT.Wyswietlanie.terminationstate_18");
            Wyswietlanie_subState = terminationstate_18;
            state_12_active = terminationstate_18;
            NOTIFY_TRANSITION_TERMINATED("ROOT.Proces.state_10.state_12.Wyswietlanie.1");
            res = eventConsumed;
        }
    
    if(res == eventNotConsumed)
        {
            res = Wyswietlanie_handleEvent();
        }
    return res;
}

void Aparat::state_11_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_11");
    NOTIFY_STATE_ENTERED("ROOT.Proces.state_10.state_11.ZapisObrazu");
    state_11_subState = ZapisObrazu;
    state_11_active = ZapisObrazu;
    //#[ state Proces.state_10.state_11.ZapisObrazu.(Entry) 
    std::cout << std::endl << "Zapis Obrazu";
    zdjecie[i]=danePrzetworzone;
    
    //#]
}

void Aparat::state_11_exit() {
    // State ZapisObrazu
    if(state_11_subState == ZapisObrazu)
        {
            NOTIFY_STATE_EXITED("ROOT.Proces.state_10.state_11.ZapisObrazu");
        }
    state_11_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Proces.state_10.state_11");
}

IOxfReactive::TakeEventStatus Aparat::state_11_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    // State ZapisObrazu
    if(state_11_active == ZapisObrazu)
        {
            if(IS_EVENT_TYPE_OF(evNoweZdjecie_Default_id))
                {
                    //## transition 17 
                    if(IS_COMPLETED(Wyswietlanie)==true)
                        {
                            NOTIFY_TRANSITION_STARTED("17");
                            NOTIFY_TRANSITION_STARTED("12");
                            NOTIFY_TRANSITION_STARTED("13");
                            state_10_exit();
                            NOTIFY_STATE_ENTERED("ROOT.Proces.terminationstate_16");
                            Proces_subState = terminationstate_16;
                            rootState_active = terminationstate_16;
                            NOTIFY_TRANSITION_TERMINATED("13");
                            NOTIFY_TRANSITION_TERMINATED("12");
                            NOTIFY_TRANSITION_TERMINATED("17");
                            res = eventConsumed;
                        }
                }
            
            
        }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedAparat::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("dane", x2String(myReal->dane));
    aomsAttributes->addAttribute("danePrzetworzone", x2String(myReal->danePrzetworzone));
}

void OMAnimatedAparat::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsParametry", true, false);
    {
        OMIterator<Parametry*> iter(myReal->itsParametry);
        while (*iter){
            aomsRelations->ADD_ITEM(*iter);
            iter++;
        }
    }
    aomsRelations->addRelation("itsPamiec", false, true);
    if(myReal->itsPamiec)
        {
            aomsRelations->ADD_ITEM(myReal->itsPamiec);
        }
    aomsRelations->addRelation("itsSterownik_2", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik_2);
}

void OMAnimatedAparat::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Aparat::Testowanie:
        {
            Testowanie_serializeStates(aomsState);
        }
        break;
        case Aparat::TrybSerwisowy:
        {
            TrybSerwisowy_serializeStates(aomsState);
        }
        break;
        case Aparat::Awaria:
        {
            Awaria_serializeStates(aomsState);
        }
        break;
        case Aparat::WyborTrybu:
        {
            WyborTrybu_serializeStates(aomsState);
        }
        break;
        case Aparat::Modernizacja:
        {
            Modernizacja_serializeStates(aomsState);
        }
        break;
        case Aparat::Diagnostyka:
        {
            Diagnostyka_serializeStates(aomsState);
        }
        break;
        case Aparat::Proces:
        {
            Proces_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedAparat::WyborTrybu_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.WyborTrybu");
}

void OMAnimatedAparat::TrybSerwisowy_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.TrybSerwisowy");
}

void OMAnimatedAparat::Testowanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Testowanie");
}

void OMAnimatedAparat::Proces_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces");
    switch (myReal->Proces_subState) {
        case Aparat::NacisniecieSpustuMigawki:
        {
            NacisniecieSpustuMigawki_serializeStates(aomsState);
        }
        break;
        case Aparat::PobranieDanych:
        {
            PobranieDanych_serializeStates(aomsState);
        }
        break;
        case Aparat::PrzetworzenieDanych:
        {
            PrzetworzenieDanych_serializeStates(aomsState);
        }
        break;
        case Aparat::state_10:
        {
            state_10_serializeStates(aomsState);
        }
        break;
        case Aparat::terminationstate_16:
        {
            terminationstate_16_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedAparat::terminationstate_16_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.terminationstate_16");
}

void OMAnimatedAparat::state_10_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.state_10");
    state_11_serializeStates(aomsState);
    state_12_serializeStates(aomsState);
}

void OMAnimatedAparat::state_12_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.state_10.state_12");
    if(myReal->state_12_subState == Aparat::Wyswietlanie)
        {
            Wyswietlanie_serializeStates(aomsState);
        }
}

void OMAnimatedAparat::Wyswietlanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.state_10.state_12.Wyswietlanie");
    switch (myReal->Wyswietlanie_subState) {
        case Aparat::Wyswietl:
        {
            Wyswietl_serializeStates(aomsState);
        }
        break;
        case Aparat::terminationstate_18:
        {
            terminationstate_18_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedAparat::Wyswietl_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.state_10.state_12.Wyswietlanie.ROOT.Wyswietlanie.Wyswietl");
}

void OMAnimatedAparat::terminationstate_18_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.state_10.state_12.Wyswietlanie.ROOT.Wyswietlanie.terminationstate_18");
}

void OMAnimatedAparat::state_11_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.state_10.state_11");
    if(myReal->state_11_subState == Aparat::ZapisObrazu)
        {
            ZapisObrazu_serializeStates(aomsState);
        }
}

void OMAnimatedAparat::ZapisObrazu_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.state_10.state_11.ZapisObrazu");
}

void OMAnimatedAparat::PrzetworzenieDanych_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.PrzetworzenieDanych");
}

void OMAnimatedAparat::PobranieDanych_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.PobranieDanych");
}

void OMAnimatedAparat::NacisniecieSpustuMigawki_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Proces.NacisniecieSpustuMigawki");
}

void OMAnimatedAparat::Modernizacja_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Modernizacja");
}

void OMAnimatedAparat::Diagnostyka_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Diagnostyka");
}

void OMAnimatedAparat::Awaria_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Awaria");
}
//#]

IMPLEMENT_REACTIVE_META_P(Aparat, Default, Default, false, OMAnimatedAparat)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Aparat.cpp
*********************************************************************/
