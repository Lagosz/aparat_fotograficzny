/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Pamiec
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Pamiec.h
*********************************************************************/

#ifndef Pamiec_H
#define Pamiec_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Pamiec
#include "Modul.h"
//## link itsSterownik_1
class Sterownik;

//## package Default

//## class Pamiec
class Pamiec : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPamiec;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Pamiec(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Pamiec();
    
    ////    Operations    ////
    
    //## operation Zapis()
    void Zapis();
    
    //## operation getOptions()
    virtual RhpString getOptions();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOptions(RhpString)
    virtual bool setOptions(const RhpString& options);
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getI() const;
    
    //## auto_generated
    void setI(int p_i);
    
    //## auto_generated
    int getZdjecie() const;
    
    //## auto_generated
    void setZdjecie(int p_zdjecie);
    
    //## auto_generated
    Sterownik* getItsSterownik_1() const;
    
    //## auto_generated
    void setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int i;		//## attribute i
    
    int zdjecie;		//## attribute zdjecie
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik_1;		//## link itsSterownik_1
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik_1();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPamiec : public OMAnimatedModul {
    DECLARE_META(Pamiec, OMAnimatedPamiec)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Pamiec.h
*********************************************************************/
