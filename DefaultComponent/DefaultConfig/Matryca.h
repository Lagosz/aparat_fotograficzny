/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Matryca
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Matryca.h
*********************************************************************/

#ifndef Matryca_H
#define Matryca_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Matryca
#include "Modul.h"
//## link itsSterownik_1
class Sterownik;

//## package Default

//## class Matryca
class Matryca : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedMatryca;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Matryca(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Matryca();
    
    ////    Operations    ////
    
    //## operation WyslijDane()
    void WyslijDane();
    
    //## operation getOptions()
    virtual RhpString getOptions();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOptions(RhpString)
    virtual bool setOptions(const RhpString& options);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik_1() const;
    
    //## auto_generated
    void setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik_1;		//## link itsSterownik_1
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik_1();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedMatryca : public OMAnimatedModul {
    DECLARE_META(Matryca, OMAnimatedMatryca)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Matryca.h
*********************************************************************/
