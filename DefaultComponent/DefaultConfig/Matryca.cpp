/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Matryca
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Matryca.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Matryca.h"
//## link itsSterownik_1
#include "Sterownik.h"
//#[ ignore
#define Default_Matryca_Matryca_SERIALIZE OM_NO_OP

#define Default_Matryca_WyslijDane_SERIALIZE OM_NO_OP

#define Default_Matryca_getOptions_SERIALIZE OM_NO_OP

#define Default_Matryca_off_SERIALIZE OM_NO_OP

#define Default_Matryca_on_SERIALIZE OM_NO_OP

#define Default_Matryca_setOptions_SERIALIZE aomsmethod->addAttribute("options", x2String(options));
//#]

//## package Default

//## class Matryca
Matryca::Matryca(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Matryca, Matryca(), 0, Default_Matryca_Matryca_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik_1 = NULL;
}

Matryca::~Matryca() {
    NOTIFY_DESTRUCTOR(~Matryca, false);
    cleanUpRelations();
}

void Matryca::WyslijDane() {
    NOTIFY_OPERATION(WyslijDane, WyslijDane(), 0, Default_Matryca_WyslijDane_SERIALIZE);
    //#[ operation WyslijDane()
    //#]
}

RhpString Matryca::getOptions() {
    NOTIFY_OPERATION(getOptions, getOptions(), 0, Default_Matryca_getOptions_SERIALIZE);
    //#[ operation getOptions()
    //#]
}

bool Matryca::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_Matryca_off_SERIALIZE);
    //#[ operation off()
    //#]
}

bool Matryca::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_Matryca_on_SERIALIZE);
    //#[ operation on()
    //#]
}

bool Matryca::setOptions(const RhpString& options) {
    NOTIFY_OPERATION(setOptions, setOptions(const RhpString&), 1, Default_Matryca_setOptions_SERIALIZE);
    //#[ operation setOptions(RhpString)
    //#]
}

Sterownik* Matryca::getItsSterownik_1() const {
    return itsSterownik_1;
}

void Matryca::setItsSterownik_1(Sterownik* p_Sterownik) {
    _setItsSterownik_1(p_Sterownik);
}

bool Matryca::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Matryca::cleanUpRelations() {
    if(itsSterownik_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
            itsSterownik_1 = NULL;
        }
}

void Matryca::__setItsSterownik_1(Sterownik* p_Sterownik) {
    itsSterownik_1 = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik_1", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
        }
}

void Matryca::_setItsSterownik_1(Sterownik* p_Sterownik) {
    __setItsSterownik_1(p_Sterownik);
}

void Matryca::_clearItsSterownik_1() {
    NOTIFY_RELATION_CLEARED("itsSterownik_1");
    itsSterownik_1 = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedMatryca::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedMatryca::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik_1", false, true);
    if(myReal->itsSterownik_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik_1);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(Matryca, Default, false, Modul, OMAnimatedModul, OMAnimatedMatryca)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Matryca.cpp
*********************************************************************/
