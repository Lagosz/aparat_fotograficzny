/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: spustMigawki
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/spustMigawki.h
*********************************************************************/

#ifndef spustMigawki_H
#define spustMigawki_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class spustMigawki
#include "Modul.h"
//## link itsSterownik_2
class Sterownik;

//## package Default

//## class spustMigawki
class spustMigawki : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedspustMigawki;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    spustMigawki(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~spustMigawki();
    
    ////    Operations    ////
    
    //## operation getOptions()
    virtual RhpString getOptions();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOptions(RhpString)
    virtual bool setOptions(const RhpString& options);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik_2() const;
    
    //## auto_generated
    void setItsSterownik_2(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik_2;		//## link itsSterownik_2
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik_2(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik_2(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik_2();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedspustMigawki : public OMAnimatedModul {
    DECLARE_META(spustMigawki, OMAnimatedspustMigawki)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/spustMigawki.h
*********************************************************************/
