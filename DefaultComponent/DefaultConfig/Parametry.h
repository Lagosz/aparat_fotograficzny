/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Parametry
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Parametry.h
*********************************************************************/

#ifndef Parametry_H
#define Parametry_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## link itsAparat
class Aparat;

//## package Default

//## class Parametry
class Parametry {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedParametry;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Parametry();
    
    //## auto_generated
    ~Parametry();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getAttribute_2() const;
    
    //## auto_generated
    void setAttribute_2(int p_attribute_2);
    
    //## auto_generated
    int getDane() const;
    
    //## auto_generated
    void setDane(int p_dane);
    
    //## auto_generated
    Aparat* getItsAparat() const;
    
    //## auto_generated
    void setItsAparat(Aparat* p_Aparat);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int attribute_2;		//## attribute attribute_2
    
    int dane;		//## attribute dane
    
    ////    Relations and components    ////
    
    Aparat* itsAparat;		//## link itsAparat
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsAparat(Aparat* p_Aparat);
    
    //## auto_generated
    void _setItsAparat(Aparat* p_Aparat);
    
    //## auto_generated
    void _clearItsAparat();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedParametry : virtual public AOMInstance {
    DECLARE_META(Parametry, OMAnimatedParametry)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Parametry.h
*********************************************************************/
