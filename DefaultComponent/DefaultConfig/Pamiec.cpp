/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Pamiec
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Pamiec.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Pamiec.h"
//## link itsSterownik_1
#include "Sterownik.h"
//#[ ignore
#define Default_Pamiec_Pamiec_SERIALIZE OM_NO_OP

#define Default_Pamiec_Zapis_SERIALIZE OM_NO_OP

#define Default_Pamiec_getOptions_SERIALIZE OM_NO_OP

#define Default_Pamiec_off_SERIALIZE OM_NO_OP

#define Default_Pamiec_on_SERIALIZE OM_NO_OP

#define Default_Pamiec_setOptions_SERIALIZE aomsmethod->addAttribute("options", x2String(options));
//#]

//## package Default

//## class Pamiec
Pamiec::Pamiec(IOxfActive* theActiveContext) : i(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Pamiec, Pamiec(), 0, Default_Pamiec_Pamiec_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik_1 = NULL;
}

Pamiec::~Pamiec() {
    NOTIFY_DESTRUCTOR(~Pamiec, false);
    cleanUpRelations();
}

void Pamiec::Zapis() {
    NOTIFY_OPERATION(Zapis, Zapis(), 0, Default_Pamiec_Zapis_SERIALIZE);
    //#[ operation Zapis()
    //#]
}

RhpString Pamiec::getOptions() {
    NOTIFY_OPERATION(getOptions, getOptions(), 0, Default_Pamiec_getOptions_SERIALIZE);
    //#[ operation getOptions()
    //#]
}

bool Pamiec::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_Pamiec_off_SERIALIZE);
    //#[ operation off()
    //#]
}

bool Pamiec::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_Pamiec_on_SERIALIZE);
    //#[ operation on()
    //#]
}

bool Pamiec::setOptions(const RhpString& options) {
    NOTIFY_OPERATION(setOptions, setOptions(const RhpString&), 1, Default_Pamiec_setOptions_SERIALIZE);
    //#[ operation setOptions(RhpString)
    //#]
}

int Pamiec::getI() const {
    return i;
}

void Pamiec::setI(int p_i) {
    i = p_i;
}

int Pamiec::getZdjecie() const {
    return zdjecie;
}

void Pamiec::setZdjecie(int p_zdjecie) {
    zdjecie = p_zdjecie;
}

Sterownik* Pamiec::getItsSterownik_1() const {
    return itsSterownik_1;
}

void Pamiec::setItsSterownik_1(Sterownik* p_Sterownik) {
    _setItsSterownik_1(p_Sterownik);
}

bool Pamiec::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Pamiec::cleanUpRelations() {
    if(itsSterownik_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
            itsSterownik_1 = NULL;
        }
}

void Pamiec::__setItsSterownik_1(Sterownik* p_Sterownik) {
    itsSterownik_1 = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik_1", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
        }
}

void Pamiec::_setItsSterownik_1(Sterownik* p_Sterownik) {
    __setItsSterownik_1(p_Sterownik);
}

void Pamiec::_clearItsSterownik_1() {
    NOTIFY_RELATION_CLEARED("itsSterownik_1");
    itsSterownik_1 = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPamiec::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("zdjecie", x2String(myReal->zdjecie));
    aomsAttributes->addAttribute("i", x2String(myReal->i));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPamiec::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik_1", false, true);
    if(myReal->itsSterownik_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik_1);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(Pamiec, Default, false, Modul, OMAnimatedModul, OMAnimatedPamiec)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Pamiec.cpp
*********************************************************************/
