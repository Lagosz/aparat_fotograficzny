/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: spustMigawki
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/spustMigawki.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "spustMigawki.h"
//## link itsSterownik_2
#include "Sterownik.h"
//#[ ignore
#define Default_spustMigawki_spustMigawki_SERIALIZE OM_NO_OP

#define Default_spustMigawki_getOptions_SERIALIZE OM_NO_OP

#define Default_spustMigawki_off_SERIALIZE OM_NO_OP

#define Default_spustMigawki_on_SERIALIZE OM_NO_OP

#define Default_spustMigawki_setOptions_SERIALIZE aomsmethod->addAttribute("options", x2String(options));
//#]

//## package Default

//## class spustMigawki
spustMigawki::spustMigawki(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(spustMigawki, spustMigawki(), 0, Default_spustMigawki_spustMigawki_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik_2 = NULL;
}

spustMigawki::~spustMigawki() {
    NOTIFY_DESTRUCTOR(~spustMigawki, false);
    cleanUpRelations();
}

RhpString spustMigawki::getOptions() {
    NOTIFY_OPERATION(getOptions, getOptions(), 0, Default_spustMigawki_getOptions_SERIALIZE);
    //#[ operation getOptions()
    //#]
}

bool spustMigawki::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_spustMigawki_off_SERIALIZE);
    //#[ operation off()
    //#]
}

bool spustMigawki::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_spustMigawki_on_SERIALIZE);
    //#[ operation on()
    //#]
}

bool spustMigawki::setOptions(const RhpString& options) {
    NOTIFY_OPERATION(setOptions, setOptions(const RhpString&), 1, Default_spustMigawki_setOptions_SERIALIZE);
    //#[ operation setOptions(RhpString)
    //#]
}

Sterownik* spustMigawki::getItsSterownik_2() const {
    return itsSterownik_2;
}

void spustMigawki::setItsSterownik_2(Sterownik* p_Sterownik) {
    _setItsSterownik_2(p_Sterownik);
}

bool spustMigawki::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void spustMigawki::cleanUpRelations() {
    if(itsSterownik_2 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_2");
            itsSterownik_2 = NULL;
        }
}

void spustMigawki::__setItsSterownik_2(Sterownik* p_Sterownik) {
    itsSterownik_2 = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik_2", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_2");
        }
}

void spustMigawki::_setItsSterownik_2(Sterownik* p_Sterownik) {
    __setItsSterownik_2(p_Sterownik);
}

void spustMigawki::_clearItsSterownik_2() {
    NOTIFY_RELATION_CLEARED("itsSterownik_2");
    itsSterownik_2 = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedspustMigawki::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedspustMigawki::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik_2", false, true);
    if(myReal->itsSterownik_2)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik_2);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(spustMigawki, Default, false, Modul, OMAnimatedModul, OMAnimatedspustMigawki)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/spustMigawki.cpp
*********************************************************************/
