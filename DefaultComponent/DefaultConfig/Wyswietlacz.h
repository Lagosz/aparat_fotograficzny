/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Wyswietlacz
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Wyswietlacz.h
*********************************************************************/

#ifndef Wyswietlacz_H
#define Wyswietlacz_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Wyswietlacz
#include "Modul.h"
//## link itsSterownik_1
class Sterownik;

//## package Default

//## class Wyswietlacz
class Wyswietlacz : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedWyswietlacz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Wyswietlacz(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Wyswietlacz();
    
    ////    Operations    ////
    
    //## operation ProcesKonies()
    void ProcesKonies();
    
    //## operation WyswietlObraz()
    void WyswietlObraz();
    
    //## operation getOptions()
    virtual RhpString getOptions();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOptions(RhpString)
    virtual bool setOptions(const RhpString& options);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik_1() const;
    
    //## auto_generated
    void setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik_1;		//## link itsSterownik_1
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedWyswietlacz : public OMAnimatedModul {
    DECLARE_META(Wyswietlacz, OMAnimatedWyswietlacz)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Wyswietlacz.h
*********************************************************************/
