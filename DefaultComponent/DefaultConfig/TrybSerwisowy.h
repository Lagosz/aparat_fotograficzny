/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TrybSerwisowy
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/TrybSerwisowy.h
*********************************************************************/

#ifndef TrybSerwisowy_H
#define TrybSerwisowy_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## class TrybSerwisowy
#include "Modul.h"
//## package Default

//## class TrybSerwisowy
class TrybSerwisowy : public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTrybSerwisowy;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    TrybSerwisowy();
    
    //## auto_generated
    virtual ~TrybSerwisowy();
    
    ////    Operations    ////
    
    //## operation getOptions()
    virtual RhpString getOptions();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOptions(RhpString)
    virtual bool setOptions(const RhpString& options);
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTrybSerwisowy : public OMAnimatedModul {
    DECLARE_META(TrybSerwisowy, OMAnimatedTrybSerwisowy)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TrybSerwisowy.h
*********************************************************************/
