/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "Aparat.h"
//## auto_generated
#include "Matryca.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "Pamiec.h"
//## auto_generated
#include "Parametry.h"
//## auto_generated
#include "spustMigawki.h"
//## auto_generated
#include "Sterownik.h"
//## auto_generated
#include "TrybSerwisowy.h"
//## auto_generated
#include "Wyswietlacz.h"
//#[ ignore
#define evOn_SERIALIZE OM_NO_OP

#define evOn_UNSERIALIZE OM_NO_OP

#define evOn_CONSTRUCTOR evOn()

#define evNacisnijMigawke_SERIALIZE OM_NO_OP

#define evNacisnijMigawke_UNSERIALIZE OM_NO_OP

#define evNacisnijMigawke_CONSTRUCTOR evNacisnijMigawke()

#define evPobierzDane_SERIALIZE OM_NO_OP

#define evPobierzDane_UNSERIALIZE OM_NO_OP

#define evPobierzDane_CONSTRUCTOR evPobierzDane()

#define evPrzekarzObraz_SERIALIZE OM_NO_OP

#define evPrzekarzObraz_UNSERIALIZE OM_NO_OP

#define evPrzekarzObraz_CONSTRUCTOR evPrzekarzObraz()

#define evPrzekazObraz_SERIALIZE OM_NO_OP

#define evPrzekazObraz_UNSERIALIZE OM_NO_OP

#define evPrzekazObraz_CONSTRUCTOR evPrzekazObraz()

#define evTest_SERIALIZE OM_NO_OP

#define evTest_UNSERIALIZE OM_NO_OP

#define evTest_CONSTRUCTOR evTest()

#define evTrybSerwisowy_SERIALIZE OM_NO_OP

#define evTrybSerwisowy_UNSERIALIZE OM_NO_OP

#define evTrybSerwisowy_CONSTRUCTOR evTrybSerwisowy()

#define evDiagnostyka_SERIALIZE OM_NO_OP

#define evDiagnostyka_UNSERIALIZE OM_NO_OP

#define evDiagnostyka_CONSTRUCTOR evDiagnostyka()

#define evModernizacja_SERIALIZE OM_NO_OP

#define evModernizacja_UNSERIALIZE OM_NO_OP

#define evModernizacja_CONSTRUCTOR evModernizacja()

#define evProces_SERIALIZE OM_NO_OP

#define evProces_UNSERIALIZE OM_NO_OP

#define evProces_CONSTRUCTOR evProces()

#define evNoweZdjecie_SERIALIZE OM_NO_OP

#define evNoweZdjecie_UNSERIALIZE OM_NO_OP

#define evNoweZdjecie_CONSTRUCTOR evNoweZdjecie()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evOn()
evOn::evOn() {
    NOTIFY_EVENT_CONSTRUCTOR(evOn)
    setId(evOn_Default_id);
}

bool evOn::isTypeOf(const short id) const {
    return (evOn_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evOn, Default, Default, evOn())

//## event evNacisnijMigawke()
evNacisnijMigawke::evNacisnijMigawke() {
    NOTIFY_EVENT_CONSTRUCTOR(evNacisnijMigawke)
    setId(evNacisnijMigawke_Default_id);
}

bool evNacisnijMigawke::isTypeOf(const short id) const {
    return (evNacisnijMigawke_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evNacisnijMigawke, Default, Default, evNacisnijMigawke())

//## event evPobierzDane()
evPobierzDane::evPobierzDane() {
    NOTIFY_EVENT_CONSTRUCTOR(evPobierzDane)
    setId(evPobierzDane_Default_id);
}

bool evPobierzDane::isTypeOf(const short id) const {
    return (evPobierzDane_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPobierzDane, Default, Default, evPobierzDane())

//## event evPrzekarzObraz()
evPrzekarzObraz::evPrzekarzObraz() {
    NOTIFY_EVENT_CONSTRUCTOR(evPrzekarzObraz)
    setId(evPrzekarzObraz_Default_id);
}

bool evPrzekarzObraz::isTypeOf(const short id) const {
    return (evPrzekarzObraz_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPrzekarzObraz, Default, Default, evPrzekarzObraz())

//## event evPrzekazObraz()
evPrzekazObraz::evPrzekazObraz() {
    NOTIFY_EVENT_CONSTRUCTOR(evPrzekazObraz)
    setId(evPrzekazObraz_Default_id);
}

bool evPrzekazObraz::isTypeOf(const short id) const {
    return (evPrzekazObraz_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPrzekazObraz, Default, Default, evPrzekazObraz())

//## event evTest()
evTest::evTest() {
    NOTIFY_EVENT_CONSTRUCTOR(evTest)
    setId(evTest_Default_id);
}

bool evTest::isTypeOf(const short id) const {
    return (evTest_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evTest, Default, Default, evTest())

//## event evTrybSerwisowy()
evTrybSerwisowy::evTrybSerwisowy() {
    NOTIFY_EVENT_CONSTRUCTOR(evTrybSerwisowy)
    setId(evTrybSerwisowy_Default_id);
}

bool evTrybSerwisowy::isTypeOf(const short id) const {
    return (evTrybSerwisowy_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evTrybSerwisowy, Default, Default, evTrybSerwisowy())

//## event evDiagnostyka()
evDiagnostyka::evDiagnostyka() {
    NOTIFY_EVENT_CONSTRUCTOR(evDiagnostyka)
    setId(evDiagnostyka_Default_id);
}

bool evDiagnostyka::isTypeOf(const short id) const {
    return (evDiagnostyka_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDiagnostyka, Default, Default, evDiagnostyka())

//## event evModernizacja()
evModernizacja::evModernizacja() {
    NOTIFY_EVENT_CONSTRUCTOR(evModernizacja)
    setId(evModernizacja_Default_id);
}

bool evModernizacja::isTypeOf(const short id) const {
    return (evModernizacja_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evModernizacja, Default, Default, evModernizacja())

//## event evProces()
evProces::evProces() {
    NOTIFY_EVENT_CONSTRUCTOR(evProces)
    setId(evProces_Default_id);
}

bool evProces::isTypeOf(const short id) const {
    return (evProces_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evProces, Default, Default, evProces())

//## event evNoweZdjecie()
evNoweZdjecie::evNoweZdjecie() {
    NOTIFY_EVENT_CONSTRUCTOR(evNoweZdjecie)
    setId(evNoweZdjecie_Default_id);
}

bool evNoweZdjecie::isTypeOf(const short id) const {
    return (evNoweZdjecie_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evNoweZdjecie, Default, Default, evNoweZdjecie())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
