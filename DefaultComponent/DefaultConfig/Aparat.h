/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Aparat
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Aparat.h
*********************************************************************/

#ifndef Aparat_H
#define Aparat_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
#include <oxf/omlist.h>
//## link itsSterownik_2
#include "Sterownik.h"
//## link itsPamiec
class Pamiec;

//## link itsParametry
class Parametry;

//## package Default

//## class Aparat
class Aparat : public OMThread, public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedAparat;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Aparat(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Aparat();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getDane() const;
    
    //## auto_generated
    void setDane(int p_dane);
    
    //## auto_generated
    int getDanePrzetworzone() const;
    
    //## auto_generated
    void setDanePrzetworzone(int p_danePrzetworzone);
    
    //## auto_generated
    Pamiec* getItsPamiec() const;
    
    //## auto_generated
    void setItsPamiec(Pamiec* p_Pamiec);
    
    //## auto_generated
    OMIterator<Parametry*> getItsParametry() const;
    
    //## auto_generated
    Parametry* newItsParametry();
    
    //## auto_generated
    void deleteItsParametry(Parametry* p_Parametry);
    
    //## auto_generated
    Sterownik* getItsSterownik_2() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int dane;		//## attribute dane
    
    int danePrzetworzone;		//## attribute danePrzetworzone
    
    ////    Relations and components    ////
    
    Pamiec* itsPamiec;		//## link itsPamiec
    
    OMList<Parametry*> itsParametry;		//## link itsParametry
    
    Sterownik itsSterownik_2;		//## link itsSterownik_2
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void _addItsParametry(Parametry* p_Parametry);
    
    //## auto_generated
    void _removeItsParametry(Parametry* p_Parametry);
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // WyborTrybu:
    //## statechart_method
    inline bool WyborTrybu_IN() const;
    
    // TrybSerwisowy:
    //## statechart_method
    inline bool TrybSerwisowy_IN() const;
    
    // Testowanie:
    //## statechart_method
    inline bool Testowanie_IN() const;
    
    // Proces:
    //## statechart_method
    inline bool Proces_IN() const;
    
    //## statechart_method
    inline bool Proces_isCompleted();
    
    //## statechart_method
    void Proces_entDef();
    
    //## statechart_method
    void Proces_exit();
    
    // terminationstate_16:
    //## statechart_method
    inline bool terminationstate_16_IN() const;
    
    // state_10:
    //## statechart_method
    inline bool state_10_IN() const;
    
    //## statechart_method
    void state_10_entDef();
    
    //## statechart_method
    void state_10_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus state_10_processEvent();
    
    // state_12:
    //## statechart_method
    inline bool state_12_IN() const;
    
    //## statechart_method
    void state_12_entDef();
    
    //## statechart_method
    void state_12_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus state_12_processEvent();
    
    // Wyswietlanie:
    //## statechart_method
    inline bool Wyswietlanie_IN() const;
    
    //## statechart_method
    inline bool Wyswietlanie_isCompleted();
    
    //## statechart_method
    void Wyswietlanie_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Wyswietlanie_handleEvent();
    
    // Wyswietl:
    //## statechart_method
    inline bool Wyswietl_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Wyswietl_handleEvent();
    
    // terminationstate_18:
    //## statechart_method
    inline bool terminationstate_18_IN() const;
    
    // state_11:
    //## statechart_method
    inline bool state_11_IN() const;
    
    //## statechart_method
    void state_11_entDef();
    
    //## statechart_method
    void state_11_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus state_11_processEvent();
    
    // ZapisObrazu:
    //## statechart_method
    inline bool ZapisObrazu_IN() const;
    
    // PrzetworzenieDanych:
    //## statechart_method
    inline bool PrzetworzenieDanych_IN() const;
    
    // PobranieDanych:
    //## statechart_method
    inline bool PobranieDanych_IN() const;
    
    // NacisniecieSpustuMigawki:
    //## statechart_method
    inline bool NacisniecieSpustuMigawki_IN() const;
    
    // Modernizacja:
    //## statechart_method
    inline bool Modernizacja_IN() const;
    
    // Diagnostyka:
    //## statechart_method
    inline bool Diagnostyka_IN() const;
    
    // Awaria:
    //## statechart_method
    inline bool Awaria_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Aparat_Enum {
        OMNonState = 0,
        WyborTrybu = 1,
        TrybSerwisowy = 2,
        Testowanie = 3,
        Proces = 4,
        terminationstate_16 = 5,
        state_10 = 6,
        state_12 = 7,
        Wyswietlanie = 8,
        Wyswietl = 9,
        terminationstate_18 = 10,
        state_11 = 11,
        ZapisObrazu = 12,
        PrzetworzenieDanych = 13,
        PobranieDanych = 14,
        NacisniecieSpustuMigawki = 15,
        Modernizacja = 16,
        Diagnostyka = 17,
        Awaria = 18
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int Proces_subState;
    
    int state_12_subState;
    
    int state_12_active;
    
    int Wyswietlanie_subState;
    
    int state_11_subState;
    
    int state_11_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedAparat : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Aparat, OMAnimatedAparat)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void WyborTrybu_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void TrybSerwisowy_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Testowanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Proces_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_16_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void state_10_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void state_12_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wyswietlanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wyswietl_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_18_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void state_11_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ZapisObrazu_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void PrzetworzenieDanych_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void PobranieDanych_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void NacisniecieSpustuMigawki_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Modernizacja_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Diagnostyka_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Awaria_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Aparat::rootState_IN() const {
    return true;
}

inline bool Aparat::WyborTrybu_IN() const {
    return rootState_subState == WyborTrybu;
}

inline bool Aparat::TrybSerwisowy_IN() const {
    return rootState_subState == TrybSerwisowy;
}

inline bool Aparat::Testowanie_IN() const {
    return rootState_subState == Testowanie;
}

inline bool Aparat::Proces_IN() const {
    return rootState_subState == Proces;
}

inline bool Aparat::Proces_isCompleted() {
    return ( IS_IN(terminationstate_16) );
}

inline bool Aparat::terminationstate_16_IN() const {
    return Proces_subState == terminationstate_16;
}

inline bool Aparat::state_10_IN() const {
    return Proces_subState == state_10;
}

inline bool Aparat::state_12_IN() const {
    return state_10_IN();
}

inline bool Aparat::Wyswietlanie_IN() const {
    return state_12_subState == Wyswietlanie;
}

inline bool Aparat::Wyswietlanie_isCompleted() {
    return ( IS_IN(terminationstate_18) );
}

inline bool Aparat::Wyswietl_IN() const {
    return Wyswietlanie_subState == Wyswietl;
}

inline bool Aparat::terminationstate_18_IN() const {
    return Wyswietlanie_subState == terminationstate_18;
}

inline bool Aparat::state_11_IN() const {
    return state_10_IN();
}

inline bool Aparat::ZapisObrazu_IN() const {
    return state_11_subState == ZapisObrazu;
}

inline bool Aparat::PrzetworzenieDanych_IN() const {
    return Proces_subState == PrzetworzenieDanych;
}

inline bool Aparat::PobranieDanych_IN() const {
    return Proces_subState == PobranieDanych;
}

inline bool Aparat::NacisniecieSpustuMigawki_IN() const {
    return Proces_subState == NacisniecieSpustuMigawki;
}

inline bool Aparat::Modernizacja_IN() const {
    return rootState_subState == Modernizacja;
}

inline bool Aparat::Diagnostyka_IN() const {
    return rootState_subState == Diagnostyka;
}

inline bool Aparat::Awaria_IN() const {
    return rootState_subState == Awaria;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Aparat.h
*********************************************************************/
