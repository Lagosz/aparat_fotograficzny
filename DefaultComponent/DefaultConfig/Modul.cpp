/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Modul.h"
//#[ ignore
#define Default_Modul_Modul_SERIALIZE OM_NO_OP

#define Default_Modul_getOptions_SERIALIZE OM_NO_OP

#define Default_Modul_off_SERIALIZE OM_NO_OP

#define Default_Modul_on_SERIALIZE OM_NO_OP

#define Default_Modul_setOptions_SERIALIZE aomsmethod->addAttribute("options", x2String(options));
//#]

//## package Default

//## class Modul
Modul::Modul() {
    NOTIFY_CONSTRUCTOR(Modul, Modul(), 0, Default_Modul_Modul_SERIALIZE);
}

Modul::~Modul() {
    NOTIFY_DESTRUCTOR(~Modul, true);
}

RhpString Modul::getOptions() {
    NOTIFY_OPERATION(getOptions, getOptions(), 0, Default_Modul_getOptions_SERIALIZE);
    //#[ operation getOptions()
    return "";
    //#]
}

bool Modul::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_Modul_off_SERIALIZE);
    //#[ operation off()
    return true;
    //#]
}

bool Modul::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_Modul_on_SERIALIZE);
    //#[ operation on()
    return true;
    //#]
}

bool Modul::setOptions(const RhpString& options) {
    NOTIFY_OPERATION(setOptions, setOptions(const RhpString&), 1, Default_Modul_setOptions_SERIALIZE);
    //#[ operation setOptions(RhpString)
    return true;
    //#]
}

int Modul::getAvability() const {
    return avability;
}

void Modul::setAvability(int p_avability) {
    avability = p_avability;
}

int Modul::getID() const {
    return ID;
}

void Modul::setID(int p_ID) {
    ID = p_ID;
}

int Modul::getNazwa() const {
    return nazwa;
}

void Modul::setNazwa(int p_nazwa) {
    nazwa = p_nazwa;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedModul::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("ID", x2String(myReal->ID));
    aomsAttributes->addAttribute("nazwa", x2String(myReal->nazwa));
    aomsAttributes->addAttribute("avability", x2String(myReal->avability));
}
//#]

IMPLEMENT_META_P(Modul, Default, Default, false, OMAnimatedModul)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/
