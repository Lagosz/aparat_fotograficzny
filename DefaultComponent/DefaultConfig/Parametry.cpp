/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Parametry
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Parametry.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Parametry.h"
//## link itsAparat
#include "Aparat.h"
//#[ ignore
#define Default_Parametry_Parametry_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Parametry
Parametry::Parametry() : dane(0) {
    NOTIFY_CONSTRUCTOR(Parametry, Parametry(), 0, Default_Parametry_Parametry_SERIALIZE);
    itsAparat = NULL;
}

Parametry::~Parametry() {
    NOTIFY_DESTRUCTOR(~Parametry, true);
    cleanUpRelations();
}

int Parametry::getAttribute_2() const {
    return attribute_2;
}

void Parametry::setAttribute_2(int p_attribute_2) {
    attribute_2 = p_attribute_2;
}

int Parametry::getDane() const {
    return dane;
}

void Parametry::setDane(int p_dane) {
    dane = p_dane;
}

Aparat* Parametry::getItsAparat() const {
    return itsAparat;
}

void Parametry::setItsAparat(Aparat* p_Aparat) {
    if(p_Aparat != NULL)
        {
            p_Aparat->_addItsParametry(this);
        }
    _setItsAparat(p_Aparat);
}

void Parametry::cleanUpRelations() {
    if(itsAparat != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsAparat");
            Aparat* current = itsAparat;
            if(current != NULL)
                {
                    current->_removeItsParametry(this);
                }
            itsAparat = NULL;
        }
}

void Parametry::__setItsAparat(Aparat* p_Aparat) {
    itsAparat = p_Aparat;
    if(p_Aparat != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsAparat", p_Aparat, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsAparat");
        }
}

void Parametry::_setItsAparat(Aparat* p_Aparat) {
    if(itsAparat != NULL)
        {
            itsAparat->_removeItsParametry(this);
        }
    __setItsAparat(p_Aparat);
}

void Parametry::_clearItsAparat() {
    NOTIFY_RELATION_CLEARED("itsAparat");
    itsAparat = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedParametry::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("dane", x2String(myReal->dane));
    aomsAttributes->addAttribute("attribute_2", x2String(myReal->attribute_2));
}

void OMAnimatedParametry::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsAparat", false, true);
    if(myReal->itsAparat)
        {
            aomsRelations->ADD_ITEM(myReal->itsAparat);
        }
}
//#]

IMPLEMENT_META_P(Parametry, Default, Default, false, OMAnimatedParametry)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Parametry.cpp
*********************************************************************/
