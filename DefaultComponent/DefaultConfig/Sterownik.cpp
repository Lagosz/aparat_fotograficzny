/********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Sterownik.h"
//## link itsAparat_2
#include "Aparat.h"
//#[ ignore
#define Default_Sterownik_Sterownik_SERIALIZE OM_NO_OP

#define Default_Sterownik_PrzetworzDane_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Sterownik
Sterownik::Sterownik(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, Default_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsWyswietlacz.setShouldDelete(false);
        }
        {
            itsMatryca_1.setShouldDelete(false);
        }
        {
            itsWyswietlacz_2.setShouldDelete(false);
        }
        {
            itsSpustMigawki_2.setShouldDelete(false);
        }
        {
            itsPamiec_1.setShouldDelete(false);
        }
    }
    itsAparat_2 = NULL;
    initRelations();
}

Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cleanUpRelations();
}

void Sterownik::PrzetworzDane() {
    NOTIFY_OPERATION(PrzetworzDane, PrzetworzDane(), 0, Default_Sterownik_PrzetworzDane_SERIALIZE);
    //#[ operation PrzetworzDane()
    //#]
}

Aparat* Sterownik::getItsAparat_2() const {
    return itsAparat_2;
}

void Sterownik::setItsAparat_2(Aparat* p_Aparat) {
    _setItsAparat_2(p_Aparat);
}

Matryca* Sterownik::getItsMatryca_1() const {
    return (Matryca*) &itsMatryca_1;
}

Pamiec* Sterownik::getItsPamiec_1() const {
    return (Pamiec*) &itsPamiec_1;
}

spustMigawki* Sterownik::getItsSpustMigawki_2() const {
    return (spustMigawki*) &itsSpustMigawki_2;
}

TrybSerwisowy* Sterownik::getItsTrybSerwisowy() const {
    return (TrybSerwisowy*) &itsTrybSerwisowy;
}

Wyswietlacz* Sterownik::getItsWyswietlacz() const {
    return (Wyswietlacz*) &itsWyswietlacz;
}

Wyswietlacz* Sterownik::getItsWyswietlacz_2() const {
    return (Wyswietlacz*) &itsWyswietlacz_2;
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsMatryca_1.startBehavior();
    done &= itsPamiec_1.startBehavior();
    done &= itsSpustMigawki_2.startBehavior();
    done &= itsWyswietlacz.startBehavior();
    done &= itsWyswietlacz_2.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Sterownik::initRelations() {
    itsMatryca_1._setItsSterownik_1(this);
    itsPamiec_1._setItsSterownik_1(this);
    itsSpustMigawki_2._setItsSterownik_2(this);
}

void Sterownik::cleanUpRelations() {
    if(itsAparat_2 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsAparat_2");
            itsAparat_2 = NULL;
        }
}

void Sterownik::__setItsAparat_2(Aparat* p_Aparat) {
    itsAparat_2 = p_Aparat;
    if(p_Aparat != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsAparat_2", p_Aparat, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsAparat_2");
        }
}

void Sterownik::_setItsAparat_2(Aparat* p_Aparat) {
    __setItsAparat_2(p_Aparat);
}

void Sterownik::_clearItsAparat_2() {
    NOTIFY_RELATION_CLEARED("itsAparat_2");
    itsAparat_2 = NULL;
}

void Sterownik::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsWyswietlacz.setActiveContext(theActiveContext, false);
        itsMatryca_1.setActiveContext(theActiveContext, false);
        itsWyswietlacz_2.setActiveContext(theActiveContext, false);
        itsSpustMigawki_2.setActiveContext(theActiveContext, false);
        itsPamiec_1.setActiveContext(theActiveContext, false);
    }
}

void Sterownik::destroy() {
    itsMatryca_1.destroy();
    itsPamiec_1.destroy();
    itsSpustMigawki_2.destroy();
    itsWyswietlacz.destroy();
    itsWyswietlacz_2.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsWyswietlacz", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsWyswietlacz);
    aomsRelations->addRelation("itsAparat_2", false, true);
    if(myReal->itsAparat_2)
        {
            aomsRelations->ADD_ITEM(myReal->itsAparat_2);
        }
    aomsRelations->addRelation("itsMatryca_1", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsMatryca_1);
    aomsRelations->addRelation("itsWyswietlacz_2", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsWyswietlacz_2);
    aomsRelations->addRelation("itsSpustMigawki_2", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSpustMigawki_2);
    aomsRelations->addRelation("itsPamiec_1", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPamiec_1);
    aomsRelations->addRelation("itsTrybSerwisowy", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsTrybSerwisowy);
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Sterownik, Default, Default, false, OMAnimatedSterownik)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/
