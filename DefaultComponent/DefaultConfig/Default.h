/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class Aparat;

//## auto_generated
class Matryca;

//## auto_generated
class Modul;

//## auto_generated
class Pamiec;

//## auto_generated
class Parametry;

//## auto_generated
class Sterownik;

//## auto_generated
class TrybSerwisowy;

//## auto_generated
class Wyswietlacz;

//## auto_generated
class spustMigawki;

//#[ ignore
#define evOn_Default_id 18601

#define evNacisnijMigawke_Default_id 18602

#define evPobierzDane_Default_id 18603

#define evPrzekarzObraz_Default_id 18604

#define evPrzekazObraz_Default_id 18605

#define evTest_Default_id 18606

#define evTrybSerwisowy_Default_id 18607

#define evDiagnostyka_Default_id 18608

#define evModernizacja_Default_id 18609

#define evProces_Default_id 18610

#define evNoweZdjecie_Default_id 18611
//#]

//## package Default



//## event evOn()
class evOn : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevOn;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evOn();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevOn : virtual public AOMEvent {
    DECLARE_META_EVENT(evOn)
};
//#]
#endif // _OMINSTRUMENT

//## event evNacisnijMigawke()
class evNacisnijMigawke : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevNacisnijMigawke;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evNacisnijMigawke();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevNacisnijMigawke : virtual public AOMEvent {
    DECLARE_META_EVENT(evNacisnijMigawke)
};
//#]
#endif // _OMINSTRUMENT

//## event evPobierzDane()
class evPobierzDane : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPobierzDane;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPobierzDane();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPobierzDane : virtual public AOMEvent {
    DECLARE_META_EVENT(evPobierzDane)
};
//#]
#endif // _OMINSTRUMENT

//## event evPrzekarzObraz()
class evPrzekarzObraz : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPrzekarzObraz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPrzekarzObraz();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPrzekarzObraz : virtual public AOMEvent {
    DECLARE_META_EVENT(evPrzekarzObraz)
};
//#]
#endif // _OMINSTRUMENT

//## event evPrzekazObraz()
class evPrzekazObraz : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPrzekazObraz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPrzekazObraz();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPrzekazObraz : virtual public AOMEvent {
    DECLARE_META_EVENT(evPrzekazObraz)
};
//#]
#endif // _OMINSTRUMENT

//## event evTest()
class evTest : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevTest;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evTest();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevTest : virtual public AOMEvent {
    DECLARE_META_EVENT(evTest)
};
//#]
#endif // _OMINSTRUMENT

//## event evTrybSerwisowy()
class evTrybSerwisowy : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevTrybSerwisowy;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evTrybSerwisowy();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevTrybSerwisowy : virtual public AOMEvent {
    DECLARE_META_EVENT(evTrybSerwisowy)
};
//#]
#endif // _OMINSTRUMENT

//## event evDiagnostyka()
class evDiagnostyka : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDiagnostyka;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDiagnostyka();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDiagnostyka : virtual public AOMEvent {
    DECLARE_META_EVENT(evDiagnostyka)
};
//#]
#endif // _OMINSTRUMENT

//## event evModernizacja()
class evModernizacja : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevModernizacja;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evModernizacja();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevModernizacja : virtual public AOMEvent {
    DECLARE_META_EVENT(evModernizacja)
};
//#]
#endif // _OMINSTRUMENT

//## event evProces()
class evProces : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevProces;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evProces();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevProces : virtual public AOMEvent {
    DECLARE_META_EVENT(evProces)
};
//#]
#endif // _OMINSTRUMENT

//## event evNoweZdjecie()
class evNoweZdjecie : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevNoweZdjecie;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evNoweZdjecie();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevNoweZdjecie : virtual public AOMEvent {
    DECLARE_META_EVENT(evNoweZdjecie)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
