/*********************************************************************
	Rhapsody	: 8.2.1 
	Login		: fewfe
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Tue, 25, Sep 2018  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsMatryca_1
#include "Matryca.h"
//## link itsPamiec_1
#include "Pamiec.h"
//## link itsSpustMigawki_2
#include "spustMigawki.h"
//## classInstance itsTrybSerwisowy
#include "TrybSerwisowy.h"
//## classInstance itsWyswietlacz
#include "Wyswietlacz.h"
//## link itsAparat_2
class Aparat;

//## package Default

//## class Sterownik
class Sterownik : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Sterownik();
    
    ////    Operations    ////
    
    //## operation PrzetworzDane()
    void PrzetworzDane();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Aparat* getItsAparat_2() const;
    
    //## auto_generated
    void setItsAparat_2(Aparat* p_Aparat);
    
    //## auto_generated
    Matryca* getItsMatryca_1() const;
    
    //## auto_generated
    Pamiec* getItsPamiec_1() const;
    
    //## auto_generated
    spustMigawki* getItsSpustMigawki_2() const;
    
    //## auto_generated
    TrybSerwisowy* getItsTrybSerwisowy() const;
    
    //## auto_generated
    Wyswietlacz* getItsWyswietlacz() const;
    
    //## auto_generated
    Wyswietlacz* getItsWyswietlacz_2() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Aparat* itsAparat_2;		//## link itsAparat_2
    
    Matryca itsMatryca_1;		//## link itsMatryca_1
    
    Pamiec itsPamiec_1;		//## link itsPamiec_1
    
    spustMigawki itsSpustMigawki_2;		//## link itsSpustMigawki_2
    
    TrybSerwisowy itsTrybSerwisowy;		//## classInstance itsTrybSerwisowy
    
    Wyswietlacz itsWyswietlacz;		//## classInstance itsWyswietlacz
    
    Wyswietlacz itsWyswietlacz_2;		//## link itsWyswietlacz_2
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsAparat_2(Aparat* p_Aparat);
    
    //## auto_generated
    void _setItsAparat_2(Aparat* p_Aparat);
    
    //## auto_generated
    void _clearItsAparat_2();
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_META(Sterownik, OMAnimatedSterownik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/
