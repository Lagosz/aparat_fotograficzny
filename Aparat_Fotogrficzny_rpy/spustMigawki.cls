I-Logix-RPY-Archive version 8.14.0 C++ 9810313
{ IClass 
	- _id = GUID 90915f9e-116c-43b6-9aff-1d8dc10269b1;
	- _myState = 8192;
	- _name = "spustMigawki";
	- _modifiedTimeWeak = 9.24.2018::23:47:14;
	- _theMainDiagram = { IHandle 
		- _m2Class = "IDiagram";
		- _name = "Model1";
		- _id = GUID 3b3b4447-3179-4752-bab5-fc4202bde62a;
	}
	- weakCGTime = 9.24.2018::23:47:14;
	- strongCGTime = 9.15.2018::21:23:1;
	- Operations = { IRPYRawContainer 
		- size = 6;
		- value = 
		{ IReception 
			- _id = GUID 71fad71c-221f-4136-a08d-967c0ffd7b60;
			- _myState = 8192;
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _virtual = 0;
			- Args = { IRPYRawContainer 
				- size = 0;
			}
			- _event = { IHandle 
				- _m2Class = "IEvent";
				- _filename = ".\\Default.sbs";
				- _subsystem = "Default";
				- _name = "evOn()";
				- _id = GUID dd19ea05-069f-4048-a50c-3f7b9b36efbf;
			}
		}
		{ IReception 
			- _id = GUID ff86fdaf-9bc7-4a49-9b83-5beca8348127;
			- _myState = 8192;
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _virtual = 0;
			- Args = { IRPYRawContainer 
				- size = 0;
			}
			- _event = { IHandle 
				- _m2Class = "IEvent";
				- _filename = ".\\Default.sbs";
				- _subsystem = "Default";
				- _name = "evNacisnijMigawke()";
				- _id = GUID dfa9a4d6-d8e1-4a1e-833a-936c276399f4;
			}
		}
		{ IPrimitiveOperation 
			- _id = GUID f376ba8d-d1f4-42ba-ba7b-4aee8f59790c;
			- _myState = 8192;
			- _name = "on";
			- _modifiedTimeWeak = 9.15.2018::23:17:27;
			- _virtual = 1;
			- Args = { IRPYRawContainer 
				- size = 0;
			}
			- _returnType = { IHandle 
				- _m2Class = "IType";
				- _subsystem = "PredefinedTypesCpp";
				- _name = "bool";
				- _id = GUID e6c263cc-4bc5-4a11-b32d-02148c93add0;
			}
			- _abstract = 0;
			- _final = 0;
			- _concurrency = Sequential;
			- _protection = iPublic;
			- _static = 0;
			- _constant = 0;
			- _itsBody = { IBody 
				- _bodyData = "";
			}
		}
		{ IPrimitiveOperation 
			- _id = GUID 7263a3da-3be0-479e-b26f-0b04dae27600;
			- _myState = 8192;
			- _name = "off";
			- _modifiedTimeWeak = 9.15.2018::23:17:27;
			- _virtual = 1;
			- Args = { IRPYRawContainer 
				- size = 0;
			}
			- _returnType = { IHandle 
				- _m2Class = "IType";
				- _subsystem = "PredefinedTypesCpp";
				- _name = "bool";
				- _id = GUID e6c263cc-4bc5-4a11-b32d-02148c93add0;
			}
			- _abstract = 0;
			- _final = 0;
			- _concurrency = Sequential;
			- _protection = iPublic;
			- _static = 0;
			- _constant = 0;
			- _itsBody = { IBody 
				- _bodyData = "";
			}
		}
		{ IPrimitiveOperation 
			- _id = GUID 5e7609ea-b9e7-4c47-8c5c-8d6794b3ea9e;
			- _myState = 8192;
			- _name = "setOptions";
			- _modifiedTimeWeak = 9.15.2018::23:17:27;
			- _lastID = 1;
			- _virtual = 1;
			- Args = { IRPYRawContainer 
				- size = 1;
				- value = 
				{ IArgument 
					- _id = GUID 5f228574-d20e-4d6f-a88f-536394b52d6d;
					- _myState = 8192;
					- _name = "options";
					- _modifiedTimeWeak = 9.15.2018::22:26:16;
					- _typeOf = { IHandle 
						- _m2Class = "IType";
						- _subsystem = "PredefinedTypes";
						- _name = "RhpString";
						- _id = GUID ae5e3720-4e3e-40f1-9346-9a8b4e501f35;
					}
					- _isOrdered = 0;
					- _argumentDirection = In;
				}
			}
			- _returnType = { IHandle 
				- _m2Class = "IType";
				- _subsystem = "PredefinedTypesCpp";
				- _name = "bool";
				- _id = GUID e6c263cc-4bc5-4a11-b32d-02148c93add0;
			}
			- _abstract = 0;
			- _final = 0;
			- _concurrency = Sequential;
			- _protection = iPublic;
			- _static = 0;
			- _constant = 0;
			- _itsBody = { IBody 
				- _bodyData = "";
			}
		}
		{ IPrimitiveOperation 
			- _id = GUID 27feaa32-a27f-485d-b31c-0fca0ab3e57d;
			- _myState = 8192;
			- _name = "getOptions";
			- _modifiedTimeWeak = 9.15.2018::23:17:27;
			- _virtual = 1;
			- Args = { IRPYRawContainer 
				- size = 0;
			}
			- _returnType = { IHandle 
				- _m2Class = "IType";
				- _subsystem = "PredefinedTypes";
				- _name = "RhpString";
				- _id = GUID ae5e3720-4e3e-40f1-9346-9a8b4e501f35;
			}
			- _abstract = 0;
			- _final = 0;
			- _concurrency = Sequential;
			- _protection = iPublic;
			- _static = 0;
			- _constant = 0;
			- _itsBody = { IBody 
				- _bodyData = "";
			}
		}
	}
	- Inheritances = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IGeneralization 
			- _id = GUID 52513185-2839-4d44-b02b-406c3c45afb9;
			- _modifiedTimeWeak = 1.2.1990::0:0:0;
			- _dependsOn = { INObjectHandle 
				- _m2Class = "IClass";
				- _filename = ".\\Default.sbs";
				- _subsystem = "Default";
				- _name = "Modul";
				- _id = GUID 1764b1ca-c821-4a34-88a4-199759c5a712;
			}
			- _inheritanceType = iPublic;
			- _isVirtual = 0;
		}
	}
	- _multiplicity = "";
	- _itsStateChart = { IHandle 
		- _m2Class = "";
	}
	- Associations = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IAssociationEnd 
			- _id = GUID 86087aa8-ff7f-443f-8ab5-3c1dc8789c05;
			- _myState = 10240;
			- _name = "itsSterownik_2";
			- _modifiedTimeWeak = 9.15.2018::23:29:52;
			- _multiplicity = "1";
			- _otherClass = { IClassifierHandle 
				- _m2Class = "IClass";
				- _filename = ".\\Default.sbs";
				- _subsystem = "Default";
				- _name = "Sterownik";
				- _id = GUID 4bca2c37-dec3-48ad-b9a3-bbf69238536b;
			}
			- _inverse = { IHandle 
				- _m2Class = "IAssociationEnd";
				- _filename = ".\\Default.sbs";
				- _subsystem = "Default";
				- _class = "Sterownik";
				- _name = "itsSpustMigawki_2";
				- _id = GUID 15fd6ca9-d8ca-4e1e-88a0-4f6202fb2d73;
			}
			- _linkName = "";
			- _linkType = Assoc;
			- _navigability = Navigable;
		}
	}
	- _classModifier = Unspecified;
}

